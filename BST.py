from node import Node

class BinarySearchTree():
    def __init__(self):
        self.root_node = None

    def insertNode(self, data):
        node = Node(data)
        parent = None
        if not self.root_node:
            self.root_node = node
        else:
            current = self.root_node
            while current:
                parent = current
                if current.data < node.data:
                    current = current.rightNode
                else:
                    current = current.leftNode
            if parent.data < node.data:
                parent.rightNode = node
            else:
                parent.leftNode = node


    def deleteNode(self, data):
        parent, currentNode = self.getNodeParent(data)
        if parent:
            if currentNode.leftNode is None and currentNode.rightNode is None:
                if parent.leftNode is currentNode:
                    parent.leftNode = None
                else:
                    parent.rightNode = None
            elif currentNode.leftNode and currentNode.rightNode:
                leftMostNode = currentNode.rightNode
                leftNodeParent = currentNode
                while leftMostNode.leftNode:
                    leftNodeParent = leftMostNode
                    leftMostNode = leftMostNode.leftNode
                if leftMostNode.rightNode:
                    leftNodeParent.leftNode = leftMostNode.rightNode
                else:
                    leftNodeParent.leftNode = None
                
                currentNode.data =  leftMostNode.data
            else:
                if parent.leftNode is currentNode:
                    if currentNode.leftNode:
                        parent.leftNode = currentNode.leftNode
                    else:
                        parent.leftNode = currentNode.rightNode
                else:
                    if currentNode.leftNode:
                        parent.rightNode = currentNode.leftNode
                    else:
                        parent.rightNode = currentNode.rightNode
        
        else:
            self.root_node = None

    def search(self, data):
        current = self.root_node
        while current and current.data != data:
            if current.data < data:
                current = current.rightNode
            else:
                current = current.leftNode
        return current

    def getNodeParent(self, data):
        parent = None
        current = None
        if not self.root_node:
            return (parent, current)
        elif self.root_node.data == data:
            current = self.root_node
            return (parent, current)
        else:
            current = self.root_node
            while current and current.data != data:
                parent = current
                if current.data < data:
                    current = current.rightNode
                else:
                    current = current.leftNode
            return (parent, current)

    def preorder(self, node="root"):
        if node == "root":
            node = self.root_node
        if node is None:
            return
        print(node.data)
        self.preorder(node.leftNode)
        self.preorder(node.rightNode)

    def postorder(self, node="root"):
        if node == "root":
            node = self.root_node
        if node is None:
            return

        self.postorder(node.leftNode)
        self.postorder(node.rightNode)
        print(node.data)

    def inorder(self, node="root"):
        if node == "root":
            node = self.root_node
        if node is None:
            return
        self.inorder(node.leftNode)
        print(node.data)
        self.inorder(node.rightNode)

    

bina = BinarySearchTree()
bina.insertNode(8)
bina.insertNode(3)
bina.insertNode(2)
bina.insertNode(1)
bina.insertNode(6)
bina.insertNode(4)
bina.insertNode(5)
bina.insertNode(7)
query = bina.search(8)
if query:
    print(query.data)
else:
    print("Data doesn't exist")


