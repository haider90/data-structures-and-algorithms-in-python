def selectionSort(arr):
    local_arr = list(arr)
    arr_len = len(local_arr)
    for i in range(0, arr_len):
        selected = i
        for j in range(i, arr_len):
            if  local_arr[selected] > local_arr[j]:
                selected = j

        temp = local_arr[selected]
        local_arr[selected] = local_arr[i]
        local_arr[i] = temp
    return local_arr


usorted_array = [99, 88, 77, 66, 55, 44, 33, 22, 11, 0]
print("Unsorted List: ", usorted_array)
print("Sorted List: ", selectionSort(usorted_array))


