"""
    worst case value is O(n2) n Squared and its best case is O(n).
"""

def Insertion_Sort(arr):
    local_arr = list(arr)
    for i in range(1, len(local_arr)):
        currentIndex = i
        insertValue = local_arr[i]
        while currentIndex > 0 and local_arr[currentIndex - 1] > insertValue:
            local_arr[currentIndex] = local_arr[currentIndex - 1]
            currentIndex -= 1
        local_arr[currentIndex] = insertValue

    return local_arr


unsorted_array = [99, 88, 77, 66, 55, 44, 33, 22, 11, 0]
print("Unsorted array: ", unsorted_array)
print("Sorted array: ", Insertion_Sort(unsorted_array))