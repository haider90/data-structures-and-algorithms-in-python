class Node:
    def __init__(self, data = None):
        self.data = data
        self.next = None

class Stack:
    def __init__(self):
        self.top = None

    def push(self, data):
        node = Node(data)
        if self.top:
            node.next = self.top
            self.top = node
        else:
            self.top = node

    def pop(self):
        if self.top:
            data = self.top.data
            if self.top.next:
                self.top = self.top.next
            else:
                self.top = None
            return data
        else:
            return "Stack Under Flow"
            
    def peek(self):
        if self.top:
            return self.top.data
        else:
            return "Stack is Empty"

stack_object = Stack()
stack_object.push(5)
stack_object.push(4)
stack_object.push(3)

print(stack_object.pop())
print(stack_object.pop())
print(stack_object.peek())
print(stack_object.pop())
print(stack_object.pop())
print(stack_object.pop())
print(stack_object.peek())

