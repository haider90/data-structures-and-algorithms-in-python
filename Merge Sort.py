def merge(list_A, list_B):
    i = j = 0
    mergedList = list()
    while i < len(list_A) and j < len(list_B):
        if list_A[i] < list_B[j]:
            mergedList.append(list_A[i])
            i += 1
        else:
            mergedList.append(list_B[j])
            j += 1
    
    while i < len(list_A):
        mergedList.append(list_A[i])
        i += 1
    
    while j < len(list_B):
        mergedList.append(list_B[j])
        j += 1
    
    return mergedList

def mergeSort(arr):
    if len(arr) == 1:
        return arr
    mid = (len(arr)) // 2
    list_A = arr[:mid]
    list_B = arr[mid:]
    mergeSortA =  mergeSort(list_A)
    mergeSortB = mergeSort(list_B)

    return merge(mergeSortA, mergeSortB)

unsorted = [99, 88, 77, 66, 55, 44, 33, 22, 11, 0]
print("Unsorted : " , unsorted)
print("Sorted: ", mergeSort(unsorted))