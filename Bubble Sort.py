"""
The bubble sort is a highly inefficient sorting algorithm with a time complexity of O(n2) {n squared}
and best case of O(n) . Generally, the bubble sort algorithm should not be used to sort large
lists. However, on relatively small lists, it performs fairly well.
"""

def Bubble_sort(array):
    local_array = list(array)
    iterations = len(local_array) - 1
    for i in range(iterations):
        for j in range(iterations):
            if local_array[j] > local_array[j+1]:
                temp = local_array[j]
                local_array[j] = local_array[j+1]
                local_array[j+1] = temp
        print("I is ", str(i) )
    return local_array

def Smart_Bubble_sort(array):
    """ No swap happens if list is already sorted
    """
    local_array = list(array)
    iterations = len(local_array) - 1
    for i in range(iterations):
        swapped = False
        for j in range(iterations):
            if local_array[j] > local_array[j+1]:
                temp = local_array[j]
                local_array[j] = local_array[j+1]
                local_array[j+1] = temp
                swapped = True
        print("I is ", str(i) )
        if not swapped:
            return local_array
    return local_array

arr = [1, 2, 3, 55, 66, 88]
print(f"unsorted List:")
print(arr)
print("Smart Bubble Sorted List:")
print(Smart_Bubble_sort(arr))

print("Bubble Sorted List:")
print(Bubble_sort(arr))