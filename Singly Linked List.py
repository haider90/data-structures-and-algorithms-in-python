class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class SinlgyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0
    
    def append(self, data):
        node = Node(data)
        if self.tail:
            self.tail.next = node
            self.tail = node
        else:
            self.head = node
            self.tail = node
        self.size += 1

    def search(self, value):
        for nodeValue in self.iter():
            if nodeValue == value:
                return True
        return False
    
    def traverse(self):
        for nodeValue in self.iter():
            print(nodeValue)
    

    def deleteNode(self, value):
        current = self.head
        previous = None
        while current:
            if current.data == value:
                if current == self.head:
                    self.head = current.next
                else:
                    previous.next = current.next
                self.size -= 1

            previous = current
            current = current.next


    def iter(self):
        current = self.head
        while current:
            value = current.data
            current = current.next
            yield value

    def clear(self):
        self.head = None
        self.tail = None


list_object = SinlgyLinkedList()
list_object.append(5)
list_object.append(6)
list_object.append(7)
list_object.append(9)
list_object.append(10)
list_object.append(11)
print(list_object.traverse())
list_object.deleteNode(5)
print(list_object.traverse())

