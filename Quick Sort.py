"""
    Best Case is when the pivot element divides the list into two equal halves by coming exactly in the middle position.

    It’s time complexity is O(nlogn) .

    Worst Case is when the list is either arranged in ascending or descending order.Then the time complexity increases to O(n^2).
"""
def quickSort(arr, start, end):
    if start >= end:
        return
    pivot = partition(arr, start, end)
    quickSort(arr, start, pivot-1)
    quickSort(arr, pivot+1, end)

def partition(arr, start, end):
    pivot = arr[end]
    separator = start - 1 
    for i in range(start, end):
        if arr[i] < pivot:
            separator += 1
            arr[separator], arr[i] = arr[i], arr[separator]
    arr[separator+1], arr[end] = arr[end], arr[separator+1]
    return separator+1


unsorted_arr = [99, 88, 77, 66, 55, 44, 33, 22, 11, 0]
print("unsorted array: ", unsorted_arr)
quickSort(unsorted_arr, 0, len(unsorted_arr) - 1)
print("Sorted array: ", unsorted_arr)
