class Node:
    def __init__(self, data):
        self.data = data
        self.previous = None
        self.next = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def append(self, data):
        node = Node(data)
        if self.tail:
            current = self.tail
            current.next = node
            self.tail = node
            self.tail.previous = current
        else:
            self.head = node
            self.tail = node
        self.size += 1
 
    def deleteNode(self, value):
        if self.head is None:
            nodeDeleted = False
        elif self.head.data == value:
            self.head = self.head.next
            self.head.previous = None
            nodeDeleted = True
        elif self.tail.data == value:
            self.tail = self.tail.previous
            self.tail.next = None
            nodeDeleted = True
        else:
            current = self.head
            while current:
                if current.data == value:
                    current.previous.next = current.next
                    current.next.previous = current.previous
                    nodeDeleted = True
                current = current.next
        if nodeDeleted:
            self.size -= 1
    
    def iter(self, movement = "forward"):
        
        current = self.tail if movement == "backward" else self.head
        while current:
            value = current.data
            current = current.previous if movement == "backward" else current.next
            yield value
    
    def traverse_forward(self):
        for value in self.iter():
            print(value)
    
    def traverse_backward(self):
        for value in self.iter("backward"):
            print(value)

    def clear(self):
        self.head = None
        self.tail = None

    def search(self, value):
        for nodeValue in self.iter():
            if nodeValue == value:
                return True
        return False

list_object = DoublyLinkedList()
list_object.append(6)
list_object.append(7)
list_object.append(8)
list_object.append(9)
list_object.append(10)

print(list_object.search(1))
print(list_object.search(10))

