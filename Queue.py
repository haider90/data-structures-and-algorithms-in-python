class Node:
    def __init__(self, data = None):
        self.data = data
        self.next = None

class Stack:
    def __init__(self):
        self.top = None
        self.count = 0

    def push(self, data):
        node = Node(data)
        if self.top:
            node.next = self.top
            self.top = node
        else:
            self.top = node
        self.count += 1

    def pop(self):
        if self.top:
            data = self.top.data
            if self.top.next:
                self.top = self.top.next
            else:
                self.top = None
            self.count -= 1
            return data
        else:
            return "No Element Available"
            

class Node_Queue:
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0
    
    def enque(self, data):
        node = Node(data)
        if self.tail:
            self.tail.next = node
            self.tail = node
        else:
            self.head = node
            self.tail = self.head
        self.count += 1
        
    def deque(self):
        if self.head:
            if self.head is self.tail:
                data = self.head.data
                self.head = None
                self.tail = None
            else:
                data = self.head.data
                self.head = self.head.next
            self.count -= 1
            return data

        else:
            return "Queue is Empty"

class Stack_Queue:
    def __init__(self):
        self.inboundStack = Stack()
        self.outboundStack = Stack()
    
    def enque(self, data):
        self.inboundStack.push(data)
    
    def deque(self):
        if not self.outboundStack.count:
            while self.inboundStack.count:
                self.outboundStack.push(self.inboundStack.pop())
        return self.outboundStack.pop()

queue_obj = Stack_Queue()
print(queue_obj.deque())
queue_obj.enque(5)
print(queue_obj.deque())

queue_obj.enque(4)
queue_obj.enque(2)

queue_obj.enque(1)

queue_obj.enque(0)

print(queue_obj.deque())
print(queue_obj.deque())
print(queue_obj.deque())

print(queue_obj.deque())

print(queue_obj.deque())

print(queue_obj.deque())

